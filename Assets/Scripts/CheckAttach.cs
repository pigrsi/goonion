﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAttach : MonoBehaviour {

	void Update () {
		checkAttachGoos();
	}
		
	// Check whether there is a goo on the side to goo-nite to.
	private void checkAttachGoos() {
		// Check tile to the right, then above
		for (int i = 0; i < 2; ++i) {
			Vector2 direction = (i == 0 ? Vector2.right : Vector2.up);
			RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 1, 1 << LayerMask.NameToLayer("Solid"));
			if (hit) {
				GameObject hitObject = hit.collider.gameObject;
				// I am a goo...if they are also a goo we can attach
				if (hitObject.tag == this.tag) {
					attachGoo(hitObject);
				}
			}
		}
	}

	private void attachGoo (GameObject goo) {
		List<GameObject> myGroup = GetComponent<MovementScript> ().myGroup;
		List<GameObject> theirGroup = goo.GetComponent<MovementScript> ().myGroup;
		if ((myGroup != null && theirGroup != null) && myGroup == theirGroup) {
			return;
		}

		GooGroupManager groupManager = GameObject.FindWithTag ("GooGroupManager").GetComponent<GooGroupManager>();
		GameObject me = transform.gameObject;

		// If neither goo is in a group, create one
		// If only one goo is in a group, add the other to that
		// If both goos have a group, merge the groups

		if (myGroup == null && theirGroup == null) {
			groupManager.createGroup (me, goo);
		} else if (myGroup == null && theirGroup != null) {
			groupManager.addToGroup (theirGroup, me);
		} else if (myGroup != null && theirGroup == null) {
			groupManager.addToGroup (myGroup, goo);
		} else {
			groupManager.mergeGroups (myGroup, theirGroup);
		}

		groupManager.printGroups ();
	}
}
