﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooGroupManager : MonoBehaviour {

	public List<List<GameObject>> gooGroup;
	private int numGoosInScene;

	// Use this for initialization
	private void Start () {
		// Outer array size: size of max -number of groups-
		numGoosInScene = GameObject.FindGameObjectsWithTag("Goo").Length;
		gooGroup = new List<List<GameObject>> (numGoosInScene / 2);
		// Initialise each inner group
		for (int i = 0; i < numGoosInScene / 2; ++i) {
			gooGroup.Add(new List<GameObject>(numGoosInScene));
		}
	}

	void Update() {
	}

	public void printGroups() {
		string printout = "Groups updated:\n";
		int groupNum = 0;
		foreach (List<GameObject> group in gooGroup) {
			if (group.Count == 0)
				continue;
			printout += "Group " + groupNum + ": ";
			groupNum++;
			foreach (GameObject goo in group) {
				printout += goo.name + " - ";
			}
			printout += "\n";
		}
		print (printout);
	}

	public List<GameObject> createGroup(GameObject goo1, GameObject goo2) {
		// Find an empty space to put this group
		int emptyIndex = 0;
		while (gooGroup[emptyIndex].Count > 0) {
			++emptyIndex;
		}

		// Add the two goos to the new group.
		gooGroup[emptyIndex].Add(goo1);
		gooGroup[emptyIndex].Add(goo2);
		goo1.GetComponent<MovementScript> ().myGroup = gooGroup [emptyIndex];
		goo2.GetComponent<MovementScript> ().myGroup = gooGroup [emptyIndex];

		return gooGroup[emptyIndex];
	}

	public void addToGroup(List<GameObject> group, GameObject goo) {
		// Find an empty space in the group to add the goo
		if (!group.Contains (goo)) {
			group.Add (goo);
			goo.GetComponent<MovementScript> ().myGroup = group;
		}
	}

	public void mergeGroups(List<GameObject> group1, List<GameObject> group2) {
		// IMPORTANT: Make sure to update the groups for the goos inside them!

		// Update members of group2 to use group1 as their group now
		foreach (GameObject goo in group2) {
			goo.GetComponent<MovementScript> ().myGroup = group1;
		}

		group1.AddRange (group2);
		group2.Clear ();
	}

	public void removeFromGroup(List<GameObject> group, GameObject goo) {
		group.Remove (goo);
		goo.GetComponent<MovementScript> ().myGroup = null;
	}
}
