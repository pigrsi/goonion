﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

	// debug purposes
	public bool disableSideMovement = false;

	// Moving == true if I have completed ALL my moves for this turn
	private bool moving = false;
	// Have I finished any side movements I need to do for this turn?
	private bool finishedSideMovement = true;
	private enum Moves {Left=0, Right, Up, Down};
	private Moves moveType;
	// The target position of my next move.
	public float targetX;

	// Physics
	private float jumpForce = 330f;
	// Have I jumped yet on this turn? (Also used for downwards "jump")
	private bool jumped = false;
	// Have I finished any vertical movement I need to do for this turn?
	private bool finishedVerticalMovement = false;
	// Am I standing on solid ground and not moving sideways?
	public bool standing = false;
	private Rigidbody2D rigidBody;

	// Groups
	public List<GameObject> myGroup;
	public bool groupStanding = false;

	void Start () {
		rigidBody = GetComponent<Rigidbody2D> ();
		targetX = transform.position.x;
		myGroup = null;
	}
	
	void Update () {
		if (disableSideMovement)
			return;

		// Unfreeze y
		rigidBody.constraints &= ~RigidbodyConstraints2D.FreezePositionY;

		checkMovementKeys();
		move();
		checkIfStanding();
		checkGroupStanding ();
		checkMovementFinished();
	}

	// Physics stuff
	void FixedUpdate () {
		// Jump
		if (moving && moveType == Moves.Up && !jumped) {
			jumped = true;
			rigidBody.AddForce (jumpForce * Vector3.up);
		} else if (groupStanding)
			// Freeze y if group is standing
			rigidBody.constraints |= RigidbodyConstraints2D.FreezePositionY;

		groupStanding = false;
	}

	// If there's input, set target for movement for the next move
	private void checkMovementKeys() {
		float gridUnit = GameSettings.GRID_UNIT;
		// Check if left or right key is pressed (change to screen swipe later)
		bool gotInput = false;
		bool rightDown = false;
		bool upDown = false;
		bool downDown = false;

		bool leftDown = gotInput = Input.GetKeyDown(KeyCode.LeftArrow);
		if (!gotInput)
			rightDown = gotInput = Input.GetKeyDown (KeyCode.RightArrow);
		if (!gotInput)
			upDown = gotInput = Input.GetKeyDown(KeyCode.UpArrow);
		if (!gotInput)
			downDown = gotInput = Input.GetKeyDown (KeyCode.DownArrow);

		Vector3 pos = this.transform.position;

		// Move left or right
		if (!moving) {
			if (leftDown) {
				// Begin move to the left
				targetX = pos.x - gridUnit;
				moving = true;
				finishedSideMovement = false;
				moveType = Moves.Left;
			} else if (rightDown) {
				// Begin move to the right
				targetX = pos.x + gridUnit;
				moving = true;
				finishedSideMovement = false;
				moveType = Moves.Right;
			} else if (upDown) {
				// Begin jump (done by physics)
				moveType = Moves.Up;
				moving = true;
				jumped = false;
				standing = false;
				finishedVerticalMovement = false;
			} else if (downDown) {
				// Try to go down (first bit here, then left to physics)
				moveType = Moves.Down;
				moving = true;
				finishedVerticalMovement = false;
			}
		}
	}

	// Act out side movement.
	private void move() {
		if (!moving)
			return;
		else if ((moveType == Moves.Left || moveType == Moves.Right) && !finishedSideMovement) {
			// Check if I have reached target
			Vector3 pos = transform.position;
			pos.x += (targetX - pos.x) / 4;
			transform.position = pos;
		} else if (moveType == Moves.Down && !jumped) {
			jumped = true;
		}
	}

	private void checkIfStanding() {
		// TODO: The thing I land on must also be standing! (or static)
		// Check if landed
		standing = false;
		// First check - y velocity has be 0 to be standing (as physics engine would have stopped it from falling)
		if (rigidBody.velocity.y < -0.1 && rigidBody.velocity.y > 0.1) {}
		else {
			RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1, 1 << LayerMask.NameToLayer("Solid"));
			if (hit) {
				// Can cast this to a box collider as all colliders are boxes in game
				BoxCollider2D myCollider = gameObject.GetComponent<BoxCollider2D>();
				BoxCollider2D yourCollider = (BoxCollider2D) hit.collider;
				if (myCollider.transform.position.y - myCollider.size.y / 2 - 0.025 <= yourCollider.transform.position.y + yourCollider.size.y / 2) {
					// Objects ARE touching
					// Currently: If they are VERY close (because rigidbody collision pushes it back up)
					// The object you're standing on must also be standing
					GameObject obj = yourCollider.gameObject;
					if (obj.tag.Equals ("Goo")) {
						if (obj.GetComponent<MovementScript> ().standing)
							standing = true;
					} else {
						standing = true;
					}
				}
			}
		}
	}

	private void checkGroupStanding() {
		if (myGroup == null)
			return;

		// If I am standing, my whole group is standing
		if (standing && !groupStanding) {
			groupStanding = true;
			foreach (GameObject goo in myGroup) {
				goo.GetComponent<MovementScript> ().groupStanding = true;
			}
		}
	}

	private void checkMovementFinished() {
		if (!moving)
			return;

		// Movement is finished if you've finished your side movement and have landed
		Vector3 pos = transform.position;

		// Side movement
		if (moveType == Moves.Left) {
			if (pos.x < targetX + 0.015) {
				pos.x = targetX;
				finishedSideMovement = true;
			}
		} else if (moveType == Moves.Right) {
			if (pos.x > targetX - 0.015) {
				pos.x = targetX;
				finishedSideMovement = true;
			}
		} else {
			finishedSideMovement = true;
		}

		transform.position = pos;

		// Needs to be checked continually as you can finish a movement but then
		// fall of an edge and have to fall again.
		finishedVerticalMovement = false;
		// Check if vertical movement is done
		if ((moveType == Moves.Up && jumped && standing) ||
		    (moveType == Moves.Down && jumped && standing) ||
		    ((moveType == Moves.Left || moveType == Moves.Right) && finishedSideMovement && standing)) {
			finishedVerticalMovement = true;
		}

		if (finishedSideMovement && finishedVerticalMovement)
			moving = false;
	}
}
